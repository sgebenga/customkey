package bd.customkey;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Partitioner;

public class ExpenseCategoryPartitioner extends Partitioner<ExpenseCategoryKey, Writable> {
    @Override
    public int getPartition(ExpenseCategoryKey expenseCategoryKey, Writable writable, int i) {
        return expenseCategoryKey.getMonth();
    }
}
