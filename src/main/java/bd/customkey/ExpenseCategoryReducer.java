package bd.customkey;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;


import java.io.IOException;

public class ExpenseCategoryReducer extends Reducer<ExpenseCategoryKey, DoubleWritable, ExpenseCategoryKey, DoubleWritable> {


    @Override
    protected void reduce(ExpenseCategoryKey key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {

        double acum = 0;
        long count = 0;
        for (DoubleWritable value : values) {
            acum += value.get();
            count++;
        }

        DoubleWritable value = new DoubleWritable(acum / count);
        context.write(key, value);
    }
}
