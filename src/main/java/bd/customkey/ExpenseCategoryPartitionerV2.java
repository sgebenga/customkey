package bd.customkey;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Partitioner;

public class ExpenseCategoryPartitionerV2 extends Partitioner<LongWritable, Writable> {

    public int getPartition(LongWritable key, Writable value, int i) {
        return key.hashCode() % i;
    }


}
