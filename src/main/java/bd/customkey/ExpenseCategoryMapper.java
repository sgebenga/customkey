package bd.customkey;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.text.ParseException;

public class ExpenseCategoryMapper extends Mapper<LongWritable, Text, ExpenseCategoryKey, DoubleWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] csv = value.toString().split(",");
        ExpenseCategoryKey eck = new ExpenseCategoryKey();

        try {
            eck.setDate(csv[0]);
            eck.setExpense(csv[1]);
            context.write(eck, new DoubleWritable(new Double(csv[2])));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
