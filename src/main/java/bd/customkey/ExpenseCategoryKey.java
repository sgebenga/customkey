package bd.customkey;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ExpenseCategoryKey implements WritableComparable<ExpenseCategoryKey> {

    private final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy/MM/dd");

    private Calendar calendar;
    private String expense;

    public ExpenseCategoryKey() {
        this.calendar = new GregorianCalendar();
        this.expense = "";
    }

    public void readFields(DataInput dataInput) throws IOException {
        int year = dataInput.readInt();
        int month = dataInput.readInt();
        int day = dataInput.readInt();

        calendar = new GregorianCalendar();
        calendar.set(year, month - 1, day);

        fillCalendar(year, month - 1, day, 0, 0, 0);
        expense = dataInput.readUTF();
    }

    private void fillCalendar(int year, int month, int day, int hour, int minute, int second) {
        calendar.set(year, month, day, hour, minute, second);
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(calendar.get(Calendar.YEAR));
        dataOutput.writeInt(calendar.get(Calendar.MONTH + 1));
        dataOutput.writeInt(calendar.get(Calendar.DAY_OF_MONTH));
        dataOutput.writeUTF(expense);
    }

    public int compareTo(ExpenseCategoryKey o) {
        int result = calendar.compareTo(o.calendar);
        if (result == 0) {
            result = expense.compareTo(o.expense);
        }
        return result;
    }

    public void setDate(String date) throws ParseException {
        calendar.setTime(FORMAT.parse(date));
        // fillCalendar();
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public Date getDate() {
        return calendar.getTime();
    }

    public int getMonth() {
        return calendar.get(Calendar.MONTH);
    }

    public String getExpense() {
        return expense;
    }

    public String toString() {
        return FORMAT.format(calendar.getTime()) + "," + expense;
    }


}
