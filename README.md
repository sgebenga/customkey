# README #

A Hadoop 2.* application

### What is this repository for? ###

* QA Hadoop application with a custom key as a Mapper output value class and a custom partitioner
* Version : 0.1

### How do I get set up? ###

*  $ mvn clean install
* $ hadoop jar target/custom-key-1.0-SNAPSHOT.jar bd.customkey.ExpenseCategoryApp data/in data/out
*  Depends on Maven and Hadoop 2.*

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact